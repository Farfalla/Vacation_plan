<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>


<h1>Nowa wycieczka</h1>
<form id="form" action="servlet" method="POST">
    <table id="">
        <tr>
            <td>Miejsce rozpoczęcia wycieczki :</td>
            <td><input type="text" name="startPlace" id="startPlace"></td>
        </tr>
        <script>
            function init1() {
            var input1 = document.getElementById('startPlace');
            var autocomplete1 = new google.maps.places.Autocomplete(input1);
            }
            google.maps.event.addDomListener(window, 'load', init1);
        </script>
        <tr>
            <td>Cel Wycieczki :</td>
            <td><input type="text" name="destination" id="destinationPlace"></td>
        </tr>

        <tr>
            <td>Data rozpoczęcia wycieczki :</td>
            <td><input type="text" name="tripStart"></td>
        </tr>
        <tr>
            <td>Data zakończenia wycieczki :</td>
            <td><input type="text" name="tripEnd"></td>
        </tr>
        <tr>
            <td>Koszt wyżywienia:</td>
            <td><input type="text" name="foodCost"></td>
        </tr>
        <tr>
            <td>Koszt noclegów:</td>
            <td><input type="text" name="accomodationCost"></td>
        </tr>
        <tr>
            <td>Budżet :</td>
            <td><input type="text" name="budget"></td>
        </tr>
        <tr>
            <td>Koszt transportu:</td>
            <td><input type="text" name="transportCost"></td>
        </tr>
        <tr>
            <td>Rodzaj pojazdu:</td>
            <td>
                <input type="radio" name="type" value="DRIVING"> Auto<br>
                <input type="radio" name="type" value="TRANSIT"> Pociąg/autobus <br>
                <input type="radio" name="type" value="PLANE"> Samolot<br>
                <input type="radio" name="type" value="BICYCLING"> Rower <br>
                <input type="radio" name="type" value="WALKING"> Na nogach <br>
            </td>
        </tr>
        <tr>
            <td>
                <input type="hidden" value="" id="lon" name="lon">
                <input type="hidden" value="" id="lat" name="lat">
                <input type="submit" value="Zapisz">
                <input type="hidden" name="command" value="NewTrip">
            </td>
        </tr>
    </table>
</form>

<script>
    var longitude;
    var latitude;
    function initMap() {
        var input2 = document.getElementById('destinationPlace');
        var autocomplete2 = new google.maps.places.Autocomplete(input2);

        autocomplete2.addListener('place_changed', function () {
            var place = autocomplete2.getPlace();
            latitude = place.geometry.location.lat();
            longitude = place.geometry.location.lng();
            document.getElementById("lon").value = longitude;
            document.getElementById("lat").value = latitude;
        });
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAZI5HCSaJ5MjvJPuQ5jHAglaOEA5x2RKE&libraries=places&callback=initMap"
        async defer></script>
<%@ include file="go_to_home.jspf" %>



