<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<style>
    #map {
        height: 400px;
        width: 100%;
    }
    #directionMap {
        height: 400px;
        width: 100%;
    }
</style>
<body bgcolor="#80D0E0">
<div align="center">

    <h1>Wynik wyszukiwania wycieczki</h1>
    <table>
        <tr>
            <th>Miejsce rozpoczęcia wycieczki</th>
            <th>Cel Wycieczki</th>
            <th>Data rozpoczęcia wycieczki</th>
            <th>Data zakończenia wycieczki</th>
            <th>Koszt noclegów </th>
            <th>Koszt wyżywienia </th>
            <th>Budżet</th>
            <th>Koszt transportu</th>
            <th>Rodzaj transportu</th>
        </tr>
        <tr>
            <td>${trip.startPlace}</td>
            <td>${trip.destination}</td>
            <td>${trip.tripStart}</td>
            <td>${trip.tripEnd}</td>
            <td>${trip.accomodationCost}</td>
            <td>${trip.foodCost}</td>
            <td>${trip.budget}</td>
            <td>${trip.transport.transportCost}</td>
            <td>${trip.transport.type}</td>
            <td>
                <form id="form" action="servlet" method="POST">
                    <input type="hidden" name="tripId" value="${trip.id}">
                    <input type="submit" value="Dodaj atrakcje">
                    <input type="hidden" name="command" value="AddAttraction">
                </form>
            </td>
        </tr>
    </table>
</div>
<p>Twoje atrakcje:</p>
<ul>
    <c:forEach var="attraction" items="${trip.attractions}">
        <li>${attraction.name}, koszt: ${attraction.attractionCost}</li>
    </c:forEach>
</ul>
<div id="map"></div>
<p>Mapa dojazdu:</p>
<div id="directionMap"></div>
<script>
    var map;
    var latitude;
    var longitude;
    var mapCenter;
    var directionMap;
    var directionsService;
    var directionsDisplay;
    var attractions;
    function initMap() {
        latitude = parseFloat(${trip.latitude});
        longitude = parseFloat(${trip.longitude});
        mapCenter = {lat: latitude, lng: longitude};
        map = new google.maps.Map(document.getElementById('map'), {
            center: mapCenter,
            zoom: 13
        });


        directionsService = new google.maps.DirectionsService;
        directionsDisplay = new google.maps.DirectionsRenderer;

        directionMap = new google.maps.Map(document.getElementById('directionMap'), {
            center: mapCenter,
            zoom: 13
        });
        directionsDisplay.setMap(directionMap);

        directionsService.route({
            origin: "${trip.startPlace}",
            destination: "${trip.destination}",
            travelMode: "${trip.transport.type}"
        }, function(response, status) {
            if (status === 'OK') {
                directionsDisplay.setDirections(response);
            } else {
                window.alert('Directions request failed due to ' + status);
            }
        });
    }

    function createMarker(latitude, longitude) {
        var marker = new google.maps.Marker({
            map: map,
            position: {lat: latitude, lon: longitude}
        });
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAZI5HCSaJ5MjvJPuQ5jHAglaOEA5x2RKE&libraries=places&callback=initMap"
        async defer></script>
<%@ include file="go_to_home.jspf" %>
</body>

