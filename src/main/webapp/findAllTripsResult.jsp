<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<h1>Wszystkie wycieczki: </h1>
<table>
    <tr>
        <th>Miejsce rozpoczęcia wycieczki</th>
        <th>Cel Wycieczki</th>
        <th>Data rozpoczęcia wycieczki</th>
        <th>Data zakończenia wycieczki</th>
        <th>Koszt noclegów </th>
        <th>Koszt wyżywienia </th>
        <th>Budżet</th>
        <th>Koszt transportu</th>
        <th>Rodzaj transportu</th>
    </tr>
    <c:forEach var="trip" items="${trips}">
        <tr>
            <td>${trip.startPlace}</td>
            <td>${trip.destination}</td>
            <td>${trip.tripStart}</td>
            <td>${trip.tripEnd}</td>
            <td>${trip.accomodationCost}</td>
            <td>${trip.foodCost}</td>
            <td>${trip.budget}</td>
            <td>${trip.transport.transportCost}</td>
            <td>${trip.transport.type}</td>
            <td>
                <form id="form" action="servlet" method="POST">
                    <input type="hidden" name="tripId" value="${trip.id}">
                    <input type="submit" value="Dodaj atrakcje">
                    <input type="hidden" name="command" value="AddAttraction">
                </form>
            </td>
        </tr>
    </c:forEach>
</table>

</div>
<%@ include file="go_to_home.jspf" %>
</body>
