<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<style>
    #map {
        height: 400px;
        width: 100%;
    }
</style>
<h1>Dodaj atrakcje</h1>
<form id="form" action="servlet" method="POST">
    <table id="">
        <tr>
            <td>Dodaj nową atrakcje w ${trip.destination}:</td>
            <tr>
                <td>Nazwa:<input type="text" name="attractionName" id="attractionName"></td>
                <td>Koszt:<input type="text" name="attractionCost"></td>
            </tr>
            <td>
                <input type="submit" value="Zapisz">
                <input type="hidden" name="tripId" value="${trip.id}">
                <input type="hidden" name="attractionLatitude" id="lat" value="">
                <input type="hidden" name="attractionLongitude" id="lon" value="">
                <input type="hidden" name="command" value="NewAttraction">
            </td>
        </tr>

    </table>
</form>

<input type="text" name="attraction" id="attraction">
<input type="submit" value="Szukaj atrakcji" id="attractionButton">
<div id="map"></div>
<script>
    var infowindow;
    var map;
    var service;
    var latitude;
    var longitude;
    var mapCenter;
    function initMap() {
        latitude = parseFloat(${trip.latitude});
        longitude = parseFloat(${trip.longitude});
        mapCenter = {lat: latitude, lng: longitude};
        map = new google.maps.Map(document.getElementById('map'), {
            center: mapCenter,
            zoom: 13
        });

        var input = document.getElementById('attractionName');
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);
        autocomplete.setOptions({strictBounds: true});
        document.getElementById("attractionButton").addEventListener("click", function(){
            var attractionName = document.getElementById('attraction').value;
            var request = {
                location: mapCenter,
                radius: '5000',
                query: attractionName
            };

            service = new google.maps.places.PlacesService(map);
            service.textSearch(request, callback);
        });

        infowindow = new google.maps.InfoWindow();

        autocomplete.addListener('place_changed', function () {
            var place = autocomplete.getPlace();
            latitude = place.geometry.location.lat();
            longitude = place.geometry.location.lng();
            document.getElementById("lon").value = longitude;
            document.getElementById("lat").value = latitude;
        });
    }

    function callback(results, status) {
        if (status === google.maps.places.PlacesServiceStatus.OK) {
            for (var i = 0; i < results.length; i++) {
                createMarker(results[i]);
            }
        }
    }

    function createMarker(place) {
        var placeLoc = place.geometry.location;
        var marker = new google.maps.Marker({
            map: map,
            position: place.geometry.location
        });

        google.maps.event.addListener(marker, 'click', function() {
            infowindow.setContent(place.name);
            infowindow.open(map, this);
            document.getElementById('attractionName').value = place.name;
            document.getElementById("lon").value = marker.getPosition().lng();
            document.getElementById("lat").value = marker.getPosition().lat();
        });

    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAZI5HCSaJ5MjvJPuQ5jHAglaOEA5x2RKE&libraries=places&callback=initMap"
        async defer></script>
<%@ include file="go_to_home.jspf" %>