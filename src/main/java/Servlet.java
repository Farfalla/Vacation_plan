import command.FindAllTripsService;
import command.FindTripService;
import command.NewAttractionService;
import command.NewTripService;
import model.Trip;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet( name = "servlet", urlPatterns = {"/servlet"}, loadOnStartup = 1)
public class Servlet extends HttpServlet{

    @Autowired
    private NewTripService newTripService;

    @Autowired
    private NewAttractionService newAttractionService;

    @Autowired
    private FindAllTripsService findAllTrips;

    @Autowired
    private FindTripService findTripService;

    private void doPostOrGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {

        if ( "NewTrip".equals(request.getParameter("command"))) {
            try {
                newTripService.setStartPlace(request.getParameter("startPlace"));
                newTripService.setDestination(request.getParameter("destination"));
                newTripService.setStart(getDateParam(request,"tripStart"));
                newTripService.setEnd(getDateParam(request,"tripEnd"));
                newTripService.setBudget(getDoubleParam(request, "budget"));
                newTripService.setTransport(request.getParameter("type"), getDoubleParam(request,"transportCost"));
                newTripService.setLatitude(getDoubleParam(request,"lat"));
                newTripService.setLongitude(getDoubleParam(request,"lon"));
                newTripService.setFoodCost(getDoubleParam(request,"foodCost"));
                newTripService.setAccomodationCost(getDoubleParam(request,"accomodationCost"));
                newTripService.execute();
                request.setAttribute("longitude",request.getParameter("lon"));
                request.setAttribute("latitude",request.getParameter("lat"));
                request.setAttribute("destinationPlace",request.getParameter("destination"));
                request.getRequestDispatcher("/index.jsp").forward(request, response);

            } catch (Exception e) {
                throw new ServletException("ROLLBACK NewUser", e);
            }
            return;
        }

        if ( "FindAllTrips".equals(request.getParameter("command"))) {
            try {
                findAllTrips.execute();
                request.setAttribute("trips",findAllTrips.getTrips());
                request.getRequestDispatcher("/findAllTripsResult.jsp").forward(request, response);

            } catch (Exception e) {
                throw new ServletException("ROLLBACK FindAllTrips", e);
            }
            return;
        }

        if ( "FindTrip".equals(request.getParameter("command"))) {
            try {
                findTripService.setId(getIntegerParam(request,"id"));
                findTripService.execute();
                request.setAttribute("trip", findTripService.getTrip());
                request.getRequestDispatcher("/findTripResult.jsp").forward(request, response);

            } catch (Exception e) {
                throw new ServletException("ROLLBACK FindTrip", e);
            }
            return;
        }

        if("AddAttraction".equals(request.getParameter("command"))){
            try {
                findTripService.setId(getIntegerParam(request,"tripId"));
                findTripService.execute();
                request.getSession().setAttribute("trip", findTripService.getTrip());
                request.getRequestDispatcher("/newAttraction.jsp").forward(request, response);
            } catch (Exception e) {
                throw new ServletException("ROLLBACK", e);
            }
            return;
        }

        if ( "NewAttraction".equals(request.getParameter("command"))) {
            try {
                System.out.println(request.getParameter("attractionName"));
                newAttractionService.setTripId(getIntegerParam(request,"tripId"));
                newAttractionService.setName(request.getParameter("attractionName"));
                newAttractionService.setLatitude(getDoubleParam(request,"attractionLatitude"));
                newAttractionService.setLongitude(getDoubleParam(request,"attractionLongitude"));
                newAttractionService.setCost(getDoubleParam(request,"attractionCost"));
                newAttractionService.execute();
                request.getRequestDispatcher("/newAttraction.jsp").forward(request, response);
            } catch (Exception e) {
                throw new ServletException("ROLLBACK NewUser", e);
            }
            return;
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPostOrGet(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPostOrGet(request, response);
    }
    public void init(ServletConfig servletConfig) throws ServletException {
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this,
                servletConfig.getServletContext());
    }

    private Integer getIntegerParam(HttpServletRequest request, String name) {
        return new Integer(request.getParameter(name));
    }

    private Double getDoubleParam(HttpServletRequest request, String name) {
        return new Double(request.getParameter(name));
    }

    private Boolean getBooleanParam(HttpServletRequest request, String name) {
        return new Boolean(request.getParameter(name));
    }

    private Date getDateParam(HttpServletRequest request, String name) throws ParseException {
        return new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter(name));
    }

}
