package command;

import model.Transport;
import model.Trip;
import model.repository.TripRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class NewTripService {

    @Autowired
    private TripRepository tripRepository;

    Trip trip = new Trip();


    public void setStartPlace(String startPlace){ trip.setStartPlace(startPlace); }
    public void setDestination(String destination){ trip.setDestination(destination);}
    public void setStart(Date start){ trip.setTripStart(start);}
    public void setEnd(Date end){ trip.setTripEnd(end);}
    public void setFoodCost(double cost){ trip.setFoodCost(cost);}
    public void setAccomodationCost(double cost) { trip.setAccomodationCost(cost);}
    public void setBudget(double budget){ trip.setBudget(budget);}
    public void setTransport(String type, double cost){
        Transport transport = new Transport();
        transport.setType(type);
        transport.setTransportCost(cost);
        trip.setTransport(transport);
    }
    public void setLongitude(Double longitude){
        trip.setLongitude(longitude);
    }
    public void setLatitude(Double latitude){
        trip.setLatitude(latitude);
    }

    public void execute(){
        this.trip.setId(null);
        tripRepository.save(this.trip);
    }
}
