package command;

import model.Trip;
import model.repository.TripRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FindAllTripsService {

    @Autowired
    TripRepository tripRepository;

    private List<Trip> trips = new ArrayList();
    public List<Trip> getTrips() {
        return this.trips;
    }

    public void execute(){
        trips = tripRepository.findAll();
    }

}
