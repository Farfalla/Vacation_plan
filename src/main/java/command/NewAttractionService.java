package command;

import model.Attraction;
import model.Trip;
import model.repository.TripRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NewAttractionService {

    @Autowired
    private TripRepository tripRepository;

    Attraction attraction = new Attraction();
    Integer tripId;

    public void setTripId(Integer id){
        tripId = id;
    }

    public void setName(String name){
        attraction.setName(name);
    }

    public void setCost(double cost){
        attraction.setAttractionCost(cost);
    }

    public void setLatitude(Double latitude){
        attraction.setAttractionLatitude(latitude);
    }

    public void setLongitude(Double longitude){
        attraction.setAttractionLongitude(longitude);
    }

    public void execute(){
        Trip trip = tripRepository.findTripById(tripId);
        List<Attraction> attractionList = trip.getAttractions();
        attractionList.add(attraction);
        tripRepository.save(trip);
    }
}
