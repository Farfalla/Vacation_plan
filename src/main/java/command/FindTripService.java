package command;

import model.Trip;
import model.repository.TripRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FindTripService {
    @Autowired
    TripRepository tripRepository;

    private Trip trip;
    private Integer id;

    public Trip getTrip() { return this.trip;   }
    public void setId(Integer id) { this.id = id;  }

    public void execute(){
        this.trip = tripRepository.findTripById(this.id);
        // Hibernate.initialize(this.trip.getAttractions();
    }
}
