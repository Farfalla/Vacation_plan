package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;

@Data
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
public class Attraction {
    private String name;
    private Double attractionLongitude;
    private Double attractionLatitude;
    private double attractionCost;
}
