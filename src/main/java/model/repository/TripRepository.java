package model.repository;

import model.Trip;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TripRepository extends JpaRepository<Trip,Integer> {

    public Trip findTripById(Integer id);
}
