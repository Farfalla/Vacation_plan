package model;

import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "vacation_trip")
public class Trip {

    @Id
    @GeneratedValue
    private Integer id;
    private String startPlace;
    private String destination;
    private Double longitude;
    private Double latitude;
    @Temporal(TemporalType.TIMESTAMP)
    private Date tripStart;
    @Temporal(TemporalType.TIMESTAMP)
    private Date tripEnd;
    private double accomodationCost;
    private double foodCost;
    private double budget;

    @ElementCollection(fetch = FetchType.EAGER)
    List<Attraction> attractions;

    @Embedded
    Transport transport;

}
